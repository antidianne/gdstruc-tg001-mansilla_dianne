#include <iostream>
#include <string> 
#include <conio.h>
#include <time.h>
using namespace std;
string gname;

// ask the user for guild name
void guildName()
{
	cout << "input guild name: " << endl;
	cin >> gname;
}
// ask the user for size of the guild
void guildSize(int& size)
{
	cout << "input guild size: " << endl;
	cin >> size;
}
// user inputs initial members
void guildMembers(string* members, int size)
{
	//cout << sizeof(members); 
	cout << "input initial members: " << endl;
	for (int i = 0; i < size; i++)
	{
		cout << "Member: " << i + 1 << endl;
		cin >> members[i];
	}
}
//print members list
void printArray(string* members, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << members[i] << endl;
	}
}
//rename function
void renaming(string* members, int size)
{
	string rename;
	string newname;
	cout << "what do you want to rename?" << endl;
	cin >> rename;
	for (int i = 0; i < size; i++)
	{
		if (rename == members[i])
		{
			cout << "enter new name:" << endl;
			cin >> newname;
			members[i] = newname;
		}
	}
}
//add function
void adding(string* members, int& size)
{
	string addname;
	int old_size = size;

	//changing the size of the guild
	size = size + 1;

	string* members_add = new string[size];

	for (int i = 0; i < old_size; i++)
	{
		members_add[i] = members[i];
	}
	cout << "what is the name of the new member?" << endl;
	cin >> addname;
	members_add[size + 1] = addname;
	delete[] members;
	members = members_add;


}
//delete function
void deleting(string* members, int size)
{
	string deletename;
	cout << "which member are you going to delete?" << endl;
	cin >> deletename;
	for (int i = 0; i < size; i++)
	{
		if (deletename == members[i])
		{
			members[i] = "";
		}
	}
}

int main()
{	//delcarations
	int size;
	char option;

	//intial function call
	guildName();
	guildSize(size);
	cout << size;
	string* members = new string[size];
	cout << sizeof(members);
	guildMembers(members, size);
	system("pause");

	//operations
	do
	{
		cout << "what do you want to do?" << endl;
		cout << "a. print member list" << endl;
		cout << "b. rename a member" << endl;
		cout << "c. add a member" << endl;
		cout << "d. delete a member" << endl;
		cin >> option;

		switch (option)
		{
		case 'a':
			printArray(members, size);

			break;
		case 'b':
			renaming(members, size);
			break;
		case 'c':
			adding(members, size);
			break;
		case 'd':
			deleting(members, size);
			break;
		default:
			cout << "please pick a valid option" << endl;
		}
	} while (1);

	system("pause");
	return 0;
}