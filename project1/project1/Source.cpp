#include <iostream>
#include <string>
#include <ctime>
using namespace std;

int arr[10];

void randomArray(int arr[])
{
	srand(time(0));
	for (int i = 0; i < 10; i++)
	{
		arr[i] = rand() % 69 + 1;
	}
}

void print(int arr[])
{
	for (int i = 0; i < 10; i++)
	{
		cout << arr[i] << ", ";
	}
	cout << endl;
}

void sortAscending(int arr[])
{
	int temp;
	for (int i = 0; i < 10; i++)
	{
		for (int x = i + 1; x < 10; x++)
		{
			if (arr[i] > arr[x]) 
			{
				temp = arr[i];
				arr[i] = arr[x];
				arr[x] = temp;

			}
		}
		cout << arr[i] << ", ";
	}
	cout << endl;
}

void sortDescending(int arr[])
{
	int temp;
	for (int i = 0; i < 10; i++)
	{
		for (int x = i + 1; x < 10; x++)
		{
			if (arr[i] < arr[x])
			{
				temp = arr[i];
				arr[i] = arr[x];
				arr[x] = temp;
			}
		}
		cout << arr[i] << ", ";
	}
	cout << endl;
}

void search(int arr[], int input)
{
	bool found = false;
	for (int i = 0; i < 10; i++)
	{
		if (arr[i] == input)
		{
			found = true;
			cout << "It took " << i << " steps to find "<< input << endl;
			break;
		}
	}
	if (found == false) {
		cout << input << " was not found on the array" << endl;
	}
}

int main()
{
	string choice;
	int input;

	randomArray(arr);
	print(arr);

	cout << "Sort by ascending or descending? (A/D)" << endl;
	cin >> choice;

	if (choice == "a" || choice == "A")
	{
		sortAscending(arr);
	}
	
	else
	{
		sortDescending(arr);
	}

	cout << "Enter a value: " << endl;
	cin >> input;

	search(arr, input);


	system("pause");
	return 0;

}
