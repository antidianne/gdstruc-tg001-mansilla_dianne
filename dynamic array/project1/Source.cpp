#include <iostream>
#include <string>
#include <time.h>
#include "UnorderedArray.h"
using namespace std;

void main()
{
	UnorderedArray<int> grades(5);

	for (int i = 0; i <= 10; i++)
		grades.push(i);

	for (int i = 0; i < grades.getSize(); i++)
		cout << grades[i] << endl;
	
	system("pause");

}